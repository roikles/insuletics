<?php
/**
 * Downloads ACF loop
 */
?>

<h2 id="qf_brochures">Brochures</h2>

<div class="grid">
<?php
if (have_rows('downloads-brochures')) : while(have_rows('downloads-brochures')): the_row();
	$title = get_sub_field('title');
	$file = get_sub_field('file');
?>
	<div class="col--one-whole  col--one-half--m">

		<div class="single-download">

			<div class="grid">

				<div class="col--one-sixth  col--one-quarter--m">
					<a href="<?php echo $file; ?>" class="single-download__link">
						<img src="<?php assets('imgs'); ?>/download-icon.png" alt="Download <?php echo $title; ?>">
					</a>
				</div>

				<div class="col--five-sixths  col--three-quarters--m">
					<a href="<?php echo $file; ?>" class="single-download__link">
						<p class="single-download__title"><?php echo $title; ?></p>
					</a>
				</div>

			</div>

		</div>

	</div>

<?php endwhile; endif; ?>
</div>

<h2 id="qf_certifications">Certifications</h2>

<div class="grid">
<?php
if (have_rows('downloads-certifications')) : while(have_rows('downloads-certifications')): the_row();
	$title = get_sub_field('title');
	$file = get_sub_field('file');
?>
	<div class="col--one-whole  col--one-half--m">

		<div class="single-download">

			<div class="grid">

				<div class="col--one-sixth  col--one-quarter--m">
					<a href="<?php echo $file; ?>" class="single-download__link">
						<img src="<?php assets('imgs'); ?>/download-icon.png" alt="Download <?php echo $title; ?>">
					</a>
				</div>

				<div class="col--five-sixths  col--three-quarters--m">
					<a href="<?php echo $file; ?>" class="single-download__link">
						<p class="single-download__title"><?php echo $title; ?></p>
					</a>
				</div>

			</div>

		</div>

	</div>
	
<?php endwhile; endif; ?>
</div>

<h2 id="qf_policies">Policies</h2>

<div class="grid">
<?php
if (have_rows('downloads-policies')) : while(have_rows('downloads-policies')): the_row();
	$title = get_sub_field('title');
	$file = get_sub_field('file');
?>
	<div class="col--one-whole  col--one-half--m">

		<div class="single-download">

			<div class="grid">

				<div class="col--one-sixth  col--one-quarter--m">
					<a href="<?php echo $file; ?>" class="single-download__link">
						<img src="<?php assets('imgs'); ?>/download-icon.png" alt="Download <?php echo $title; ?>">
					</a>
				</div>

				<div class="col--five-sixths  col--three-quarters--m">
					<a href="<?php echo $file; ?>" class="single-download__link">
						<p class="single-download__title"><?php echo $title; ?></p>
					</a>
				</div>

			</div>

		</div>

	</div>
	
<?php endwhile; endif; ?>
</div>