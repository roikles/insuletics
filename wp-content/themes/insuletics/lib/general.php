<?php 
/**
 * Outputs sections of content for each individual case study
 */
class ProjectContent{

	function get_excerpt(){
		if (get_field('project-background',get_the_ID())) {
			$background = get_field('project-background');
			$excerpt = substr($background,0, strpos($background, "</p>")+4);
			return $excerpt;
		}
	}

	function get_description(){
		if (get_field('description',get_the_ID())) {
			$description = get_field('description');
			return $description;
		}
	}

	function get_project_background(){
		if (get_field('project-background',get_the_ID())) {
			$background = get_field('project-background');
			return $background;
		}
	}

	function get_project_challenges(){
		if (get_field('project-challenges',get_the_ID())) {
			$challenges = get_field('project-challenges');
			return $challenges;
		}
	}

	function get_project_results(){
		if (get_field('system-results',get_the_ID())) {
			$results = get_field('system-results');
			return $results;
		}
	}

	function get_location(){
		if (get_field('location')) {
			$location = get_field('location');
			return $location;
		}
	}

	function get_content(){
		if (get_field('project-background',get_the_ID())) {
			$description 	= get_field('description');
			$background 	= get_field('project-background');
			$challenges 	= get_field('project-challenges');
			$results 			= get_field('system-results');
			$content			=	$background . $challenges . $results;
			return $content;
		}
	}

}

/**
 * Check if current page is a direct descendant
 * @param  [int]  $page_id [potential parent ID]
 * @return boolean          [true or false if current page is a child of $page_id]
 */
function is_child($page_id) { 
  global $post; 
  if( is_page() && ($post->post_parent == $page_id) ) {
     return true;
  } else { 
     return false; 
  }
}


/**
 * [agency_footer is designed to add an agency footer link e.g. 'designed by XXX'
 * to a dynamic footer (included on every page). To avoid search engines picking 
 * it up as link spam the function adds rel="nofollow"] to all links except the 
 * primary page.]
 * 
 * @param  [string]  $agency_url        [the agency URL you wish to link to]
 * @param  [string]  $agency_title      [the title of the agency link (agency link e.g. 'Agency Name'. Don't be spammy e.g. Best Web Designer London)]
 * @param  [string]  $primary_url       [The URL that WILL be indexed (won't contain rel="nofollow")]
 * @param  [string]  $protocol          [*Optional* if set to http or https primary link will only work on that protocol]
 * @return [string]                     [returns an anchor e.g. <a href="http://myagency.com">Agency Name</a>]
 */

function agency_link($agency_url, $agency_title, $primary_url, $protocol = NULL){

    // current url contains the current page url (takes into account https)
    $current_url = 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	
    //strip trailing slash
    $current_url = rtrim($current_url,"/");

    // HTTP or HTTPS
    $current_protocol = 'http' . (isset($_SERVER['HTTPS']) ? 's' : '');

    // Path information of current location
    $current_path = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

    // Ignore the www prefix    
    $primary_url = str_replace("www.","",$primary_url);

    // Check for strict protocols
    if($protocol){
        // if the CURRENT protocol is the same as the defined protocol AND the primary url matches the current_url
        $nofollow = ($current_protocol == $protocol AND $current_url == $current_protocol . '://' . $current_path ? false : true );
    } else {
        $nofollow = ($primary_url == $current_url ? false : true );
    }

    // return the link with or without rel=nofollow as appropriate 
    $agency_link = '<a href="' . $agency_url . '" ' . ($nofollow == true ? 'rel="nofollow"' : '') . '>' . $agency_title .'</a>';

    return $agency_link;
}