<?php get_header(); ?>

<?php get_sidebar('main'); ?>
<div class="container">

	<!-- Static Billboard -->
	<div class="billboard-container">
		<section class="billboard  billboard--slide" style="background-image: url(<?php assets('imgs/billboards'); ?>/casestudies-billboard.jpg)">
			<div class="billboard__background"></div>
			<div class="billboard__caption">
				<div class="wrapper  wrapper--central  wrapper--thin">
					<h1 class="billboard__title"><?php post_type_archive_title(); ?></h1>
					<span class="billboard__description">Insuletics specialise in delivering the most modern thermal and aesthetic solutions for your project needs.</span>
				</div>
			</div>
		</section>
	</div>
	
	<!-- Standard content area, white bg with  text and images -->
	<section class="content  content--padded">
		<div class="wrapper  wrapper--central">

			<?php
			/**
			 * All Categories except News
			 */
				$args = array(
				 	'orderby' => 'name',
				    'hierarchical' => 1,
				    'exclude' => 3, // Exclude News Category
					'title_li' => ''
				);

				$categories = get_categories( $args );

				foreach ($categories as $key => $value) {
					$title = $value->name;

					// If is Parent category just output title
					if ($value->parent == 0) { 

						echo "<h2>".$title."</h2>";

					// If is child with posts, output post
					} else if ($value->count > 0) { 

						$cat_ID = $value->cat_ID;

						echo "<h3>".$title."</h3>";

						$query_args = array(
							'post_type' => 'casestudies',
							'category' => $cat_ID,
							'posts_per_page' => -1,
							'post_status' => 'publish'
						);

						$query = get_posts($query_args);

				?>
					<div class="grid">
					<?php
						if ($query): foreach ($query as $post): setup_postdata( $post );
							$img = wp_get_attachment_image_src( get_post_thumbnail_id( ), 'Single Project Thumb', false );
							$url = $img[0];
					?>

								<div class="col--one-half  col--one-third--l">

									<div class="project-thumb">
										<a href="<?php the_permalink(); ?>">
											<img src="<?php echo $url; ?>">
											<h3 class="project-thumb--title"><?php the_title(); ?></h3>
											<p><strong><?php if(get_field('location')){ the_field('location'); }else{ echo"<br>"; } ?></strong></p>
										</a>
										<a href="<?php the_permalink(); ?>" class="btn  btn--default">View Project</a>
									</div>

								</div>

					<?php endforeach; endif; wp_reset_postdata(); ?>
					</div>
					<?php
					}
				}
			?>
			
		</div>
	</section>

<?php get_footer(); ?>