<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo('charset'); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>
<?php wp_title('&raquo;',true,'right'); ?>
<?php bloginfo('name'); ?> - <?php bloginfo('description'); ?>
</title>
<link rel="icon" href="<?php assets('imgs'); ?>/favicon.ico" type="image/x-icon">
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/style.css?ver=1">
<!--[if (lte IE 9)]>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js"></script>
    <script type="text/javascript" src="<?php assets('js'); ?>/non-min/modernizr.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js" ></script>
<![endif]-->
<?php wp_head(); ?>
<link rel="dns-prefetch" href="http://use.typekit.net">
<script type="text/javascript" src="//use.typekit.net/bji2hao.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
</head>
<body <?php body_class(); ?>>
<header>
	<div class="trigger-container">
		<a class="mobile-nav-trigger" id="mobileNavTrigger">
			<div class="lines-button  x">
				<span class="lines"></span>
			</div>
		</a>
	</div>
</header>
<!-- Mobile Logo -->
<div class="mobile-logo">
	<img src="<?php assets('imgs'); ?>/insuletics-logo--white.svg" alt="Insuletics Ltd">
</div>