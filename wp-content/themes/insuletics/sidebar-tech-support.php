<aside class="col--one-whole  col--one-third--l  col--one-quarter--xl">
	<div class="sidebar-container">
		<!-- <nav class="secondary-nav" id="fixedSidebar"> -->
		<nav class="secondary-nav">

			<!-- secondary menu -->
			<h3>Documentation</h3>
			<ul class="sub-menu">
				<?php 
				$args 	=	array(
					'posts_per_page'	=>	-1,
					'post_type'			=>	'page',
					'order'				=>	'ASC',
					'orderby'			=>	'title',
					'post_status'		=>	'publish',
					'post_parent'		=>	12
					);

				$current_post_id 	= get_the_id();
				$posts 						= get_posts($args);

				foreach ($posts as $post) : setup_postdata( $post ); $class = "sub-menu__item"; 

				/* Check for active page */
				if (get_the_id() === $current_post_id) { $class = "sub-menu__item  sub-menu__item--active"; }
				?>

				<li class="<?php echo $class; ?>"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

				<?php
				endforeach; wp_reset_postdata();
				?>
			</ul>
		</nav>
	</div>
</aside>