<?php get_header(); ?>

<?php get_sidebar('main'); ?>
<div class="container">

	<!-- Standard content area, white bg with  text and images -->

	<section class="content  content--padded">
		<div class="wrapper  wrapper--central">

			<div class="grid">

				<div class="col--one-whole  col--three-quarters--l">
					<article class="news-article">
						<h1>The Latest News from Insuletics Ltd</h1>
						<?php if (have_posts()): while(have_posts()): the_post(); ?>
							<h2><?php the_title(); ?></h2>
							<?php the_excerpt(); ?>
							<a href="<?php the_permalink(); ?>" class="btn">Read more</a>
							<div class="date-posted  clear">
								<div class="share-links">
									<a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" class="icon-facebook" title="Share on Facebook" target="_blank"></a>
									<a href="https://twitter.com/home?status=<?php the_title(); ?>%20-%20<?php the_permalink(); ?>" class="icon-twitter" title="Share on Twitter" target="_blank"></a>
									<a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php the_permalink(); ?>&amp;title=<?php the_title(); ?>&amp;summary=<?php echo get_the_excerpt(); ?>&source=<?php echo home_url(); ?>" class="icon-linkedin" title="Share on LinkedIn" target="_blank"></a>
								</div>
								<div class="the-date">
									<p>Posted on <?php the_time('l F j, Y'); ?></p>
								</div>
							</div>
						<?php endwhile; endif; ?>
					</article>
				</div>

				<?php get_sidebar('news'); ?>

			</div>
		</div>
	</section>
<?php get_footer(); ?>