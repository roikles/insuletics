
	<section class="content  content--padded--2x  content--bg-tile">
		<div class="wrapper  wrapper--central">
			
			<section class="ft-projects--home">
				
				<div class="grid  grid--full">
					
					<div class="col--one-whole  col--one-third--l">
						
						<div class="ft-projects--home__section-title">
							<h2>Featured<span>Case Studies</span></h2>
						</div>

					</div>

				<?php 
					// Fetch selected posts from Homepage custom field
					$featured_posts = get_field('case_studies');

					if ($featured_posts) : 
						$post = $featured_posts[0];
						setup_postdata( $post ); $casestudy = new ProjectContent(); 
						$img = wp_get_attachment_image_src( get_post_thumbnail_id( ), 'Featured Case Study', false );
						$url = $img[0];
				?>
					
						<div class="col--one-whole  col--one-half--m  col--one-third--l">
							
							<div class="ft-project--home">
								<div class="ft-project--home__img" style="background-image: url(<?php echo $url; ?>)"></div>
								<div class="ft-project--home__info">
									<p class="ft-project--home__info__title"><?php the_title(); ?></p> 
									<p class="ft-project--home__info__location"><?php the_field('location'); ?></p>
									<a href="<?php the_permalink(); ?>" class="btn">View Project</a>
								</div>
							</div>

						</div>

				<?php 
						$post = $featured_posts[1];
						setup_postdata( $post ); $casestudy = new ProjectContent(); 
						$img = wp_get_attachment_image_src( get_post_thumbnail_id( ), 'Featured Case Study', false );
						$url = $img[0];
				?>
					
					<div class="col--one-whole  col--one-half--m  col--one-third--l">
						
						<div class="ft-project--home--alt">
							<div class="ft-project--home__info  ft-project--home__info--alt">
								<p class="ft-project--home__info__title"><?php the_title(); ?></p> 
								<p class="ft-project--home__info__location"><?php the_field('location'); ?></p>
								<a href="<?php the_permalink(); ?>" class="btn">View Project</a>
							</div>
							<div class="ft-project--home__img" style="background-image: url(<?php echo $url; ?>)"></div>
						</div>

					</div>

				<?php endif; wp_reset_postdata(); ?>
				
				</div>

			</section>

		</div>
	</section>