<?php get_header(); ?>

<?php get_sidebar('main'); ?>
<div class="container  clear">

	<!-- Include Billboard -->
	<?php get_template_part('includes/billboard'); ?>

	<!-- Standard content area, white bg with  text and images -->

	<section class="content  content--padded">
		<div class="wrapper  wrapper--central">

			<div class="grid">
				<div class="col--one-whole  col--two-thirds--l  col--three-quarters--xl">
					<div class="content-container">

					<?php 
					if (have_posts()): while(have_posts()): the_post(); ?>
						<h1><?php the_title(); ?></h1>
					<?php

						// Get Downloads Page ACF logic template
						if (is_page('downloads')){ get_template_part('includes/downloads'); };

						// Get ACF logic template for System Finishes page
						if (is_page(242)){ get_template_part('includes/system-finishes'); };

						the_content(); 

						// Get ACF logic template if page_id is a child of "Technical Support"
						if (is_child(12)){ get_template_part('includes/technical-files'); };

					endwhile; endif 
					?>

					</div>
				</div>

				<?php 
				// Technical Support Sidebar
				if (is_page(12) || is_child(12)){ get_sidebar('tech-support'); }; ?>

				<?php 
				// System Components sidebar
				if (is_page(10)){ get_sidebar('finishes'); }; ?>

				<?php 
				// News Sidebar
				if ((is_single() && in_category('news'))){ get_sidebar('news'); } ?>

			</div>
		</div>
	</section>

<?php get_footer(); ?>