/**
 * Scripts.js
 * User scripts and plugin instantiations
 * scripts.js should be called in the site footer
 */

 jQuery(document).ready(function($) {

 	svgeezy.init('nocheck', 'png');

 	$("head").removeClass('no-js');

 	/**
 	 * Billboard slider for Homepage
 	 */
 	var billboardSlider = $("#billboardSlider");
 	if (billboardSlider.length) {
 		billboardSlider.slick({
	 		autoplay: true,
	 		infinite: true,
	 		fade: true,
	 		autoplaySpeed: 5000,
	 		pauseOnHover: false,
	 		slide: "section",
	 		arrows: false
	 	});
 	};

	 /**
	  * Carousel slideshow for Footer
	  */
	var carousel = $('#lightCarousel');
	if (carousel.length) {
	 	/* Setup properties and initialise slider*/
	 	carousel.slick({
	 		autoplay: true,
	 		autoplaySpeed: 2500,
	 		slide: 'li',
	 		slidesToShow: 5,
	 		infinite: true,
	 		arrows: false,
	 		speed: 300,
	 		responsive: [
	 			{
	 				breakpoint: 1024,
	 				settings: {
	 					slidesToShow: 3,
	 				}
	 			},
	 			{
	 				breakpoint: 640,
	 				settings: {
	 					slidesToShow: 2,
	 				}
	 			}
	 		]
	 	});
	};

	/**
	 * Toggle 'close' class on mobile navigation trigger element and trigger slide out for nav menu
	 */
	var navTrigger = $('#mobileNavTrigger');
	navTrigger.sidr({
	 	name: 'mobileNav',
	 	source: '#sidebarSlideOut',
	 	displace: false,
	 	renaming: false,
	 	onOpen: function(){
	 		$(".lines-button").addClass('close');
	 		$("header").addClass('triggered');
	 	},
	 	onClose: function(){
	 		$(".lines-button").removeClass('close');
	 		$("header").removeClass('triggered');
	 	}
	});


	/**
	 * Smooth scroll to same-page anchors
	 * @link  http://css-tricks.com/snippets/jquery/smooth-scrolling/
	 */
	$(function() {
	 	$('a[href*=#]:not([href=#])').click(function() {
	 		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	 			var target = $(this.hash);
	 			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	 			if (target.length) {
	 				$('html,body').animate({
	 					scrollTop: target.offset().top
	 				}, 1000);
	 				return false;
	 			}
	 		}
	 	});
	});

	/**
	 * Fixed content sidebar when scrolling page at desktop view
	 */
	floatingSidebar();
	function floatingSidebar(){
		var windowWidth = $(window).outerWidth();

		// Toggle Debug output
		var debug = false;

		if ($('#fixedSidebar').length) {

			var sidebar = $("#fixedSidebar"), // Sidebar object
				sidebarY = Math.floor(sidebar.offset().top), // Sidebar Top Left position in relation to document
				sidebarHeight = sidebar.outerHeight(), // Sidebar height including any additional padding/margins
				sidebarWidth = sidebar.outerWidth(),
				sidebarContainer = $(".sidebar-container"), // Sidebar container object
				billboardHeight = 0, // Default height of Billboard set to 0 incase object does not exist
				containerHeight = Math.floor($(".content-container").innerHeight()), // Internal height of content without padding/margins
				contentHeight = Math.floor($(".content").innerHeight()); // Total height of content including padding/margins

			if (windowWidth > 1024) {

				if (sidebarHeight < Math.floor(containerHeight - sidebarHeight)) {
					// Ensure the sidebar container has the same height as the internal height of the content container
					sidebarContainer.height(containerHeight);

					if($(".billboard-container").length){ // If a billboard exists, re-assign billboardHeight with new height
						billboardHeight = $(".billboard-container").outerHeight();
					}

					var marginTop = Math.floor(sidebarY - billboardHeight), // Get the gap between the billboard and the content
						maxTravel = Math.floor(sidebarContainer.height() - sidebarHeight + billboardHeight); // Calculate the maximum height for the sidebar to travel down to


						$(window).scroll(function() {
							var scrollPos = $(window).scrollTop(); // Current pixel position from the top of the page
										
							// If the scroll position is greater than the billboard height but less than the total height of the content
							if (scrollPos >= billboardHeight && scrollPos < maxTravel) { 
								if (debug) { console.log("On the rails, guv: "+scrollPos) };	
								sidebar.css({
									'position': 'fixed',
									'top': marginTop, // Top buffer space so the sidebar doesn't stick to the top of the window
									'width': sidebarWidth
								});

							// If the scroll position is greater than the total height of the content
							} else if (scrollPos > maxTravel) { 
								if (debug) { console.log("Hit the brakes!: "+scrollPos) };	
								sidebar.css({
									'position': 'absolute',
									'top': Math.floor(sidebarContainer.height() - sidebarHeight), // Stick sidebar to bottom of content area
									'width': sidebarWidth
								});
							
							// If the scroll position is smaller than the billboardHeight then return original CSS properties to sidebar
							} else { 
								if (debug) { console.log("We ain't doin squat!: "+scrollPos) };	
								sidebar.css({
									'position': 'relative',
									'top': 0,
									'width': sidebarWidth
								});
							}
					    });
					if (debug) {
						console.log("Content container height = "+containerHeight+". Sidebar height = "+sidebarHeight);
						console.log("Content inside height is = "+contentHeight);
						console.log("The top margin of the sidebar is = "+marginTop);
						console.log("Because billboard height is = "+billboardHeight+" and sidebar positionY is = "+sidebarY);
						console.log("This means the maximum travel distance is = "+maxTravel);
						console.log("(contentContainer: "+containerHeight+" - sidebarHeight: "+sidebarHeight+")");
					}
				}
			}else{
				sidebarContainer.height(sidebarHeight);
				sidebar.css({
					'position': 'static',
					'top': 0,
					'width': 'auto'
				});
			}
		}
	}


	/* Window Resize Listener */
	$(window).resize(function(event){
		resizeContainer();
		floatingSidebar();
	});

	/* IE css calc() function fallback for IE8*/
	resizeContainer();
	function calc(selector, method, adjust) {
	    return $(selector)[method]() + adjust;
	}
	function resizeContainer(){
		var windowWidth = $(window).outerWidth();
		if ($.browser.msie  &&  $.browser.version <= 8) {
			var navSidebarWidth = $(".sidebar").outerWidth(),
		    	width = calc('body', 'width', -navSidebarWidth);

		    if (windowWidth > 1024) {
		    	$('.container').css('width', width);
		    }else{
				$('.container').css('width','100%');
		    }

		}
	}

});
