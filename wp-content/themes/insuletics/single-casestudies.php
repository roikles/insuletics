<?php get_header(); the_post(); $casestudy = new ProjectContent(); // dir: lib/general.php ?>

<?php get_sidebar('main'); ?>
<div class="container">

	<!-- Include Billboard -->
	<?php get_template_part('includes/billboard'); ?>

	<!-- Standard content area, white bg with  text and images -->

	<section class="content  content--padded">
		<div class="wrapper  wrapper--central">

			<div class="grid">

				<div class="col--one-whole  col--three-quarters--l">
					<h2 id="prj_background">Project Background</h2>
					<?php echo $casestudy->get_project_background(); ?>

					<h2 id="prj_challenges">Project Challenges</h2>
					<?php echo $casestudy->get_project_challenges(); ?>

					<h2 id="prj_results">The System &amp; Results</h2>
					<?php echo $casestudy->get_project_results(); ?>
				</div>

				<aside class="col--one-whole  col--one-quarter--l">
					<nav class="secondary-nav">
						<!-- secondary menu -->
						
						<div class="sidebar__block">
							<h3>Jump to Section</h3>
							<ul class="sub-menu">
								<li><a href="#prj_background">Project Background</a></li>
								<li><a href="#prj_challenges">Project Challenges</a></li>
								<li><a href="#prj_results">The System &amp; Results</a></li>
							</ul>
						</div>

						<div class="sidebar__block">
							<h3>Other Case Studies</h3>
							<ul class="sub-menu">
							<?php 
								$args 	=	array(
									'posts_per_page'	=>	-1,
									'post_type'				=>	'casestudies',
									'order'						=>	'ASC',
									'orderby'					=>	'title',
									'post_status'			=>	'publish'
								);

								$current_post_id 	= get_the_id();
								$posts 						= get_posts($args);
								
								foreach ($posts as $post) : setup_postdata( $post ); $class = ""; 

								/* Check for active page */
								if (get_the_id() === $current_post_id) { $class = "active-item"; }
							?>

									<li class="<?php echo $class; ?>"><a href="<?php the_permalink(); ?>"><?php the_title(); if(get_field('location')): echo ", "; the_field('location'); endif; ?></a></li>

							<?php
								endforeach; wp_reset_postdata();
							?>
							</ul>
						</div>
					</nav>
				</aside>

			</div>
		</div>
	</section>

<?php get_footer(); ?>