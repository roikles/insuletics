# Flexbones Wordpress Theme

![flexbones image](http://roikles.com/etc/flexbones-github-header.jpg "Flexbones")

Flexbones is a responsive starter theme. Flexible, simple and the CSS is under 10kb! It cleans up all the out of the box Wordpress bloat and leaves you with a clean, organised start to your project. The project has an MIT license and if you use it for anything please let me know!

## Features

* HTML5 support
* Responsive modular grid system
* IE8+ support with js polyfills for media queries and rem units.
* Gruntfile ready to compile css from Sass and concatenate and minify js 
* Typekit Support
* Typography starter kit
* Form starter kit
* Admin bar removed by default
* Images wrapped in <figure> tags instead of paragraphs
* Custom post type function
* Removes bloated Wordpress feed links etc from page head
* Remove Wordpress recent comment inline CSS
* assets() function as a shorter alternative to get_stylesheet_directory_uri()

## Demo

[Flexbones Demo](http://roikles.com/flexbones)