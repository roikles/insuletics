<?php get_header(); the_post(); ?>

<?php get_sidebar('main'); ?>
<div class="container">

	<!-- Include Billboard -->
	<?php get_template_part('includes/billboard'); ?>

	<!-- Standard content area, white bg with  text and images -->

	<?php if (have_rows('homepage_blurbs')): ?>
	<section class="content  content--padded">
		<div class="wrapper  wrapper--central">

			<div class="homepage-blurbs">
				<div class="homepage-blurbs__text">
					<div class="grid">
						<?php while(have_rows('homepage_blurbs')): the_row(); ?>
							<div class="col--one-whole  col--one-third--m">
								<h3><?php the_sub_field('heading') ?></h3>
								<?php the_sub_field('text') ?>
								<a href="<?php the_sub_field('link'); ?>" class="btn">More Info</a>
							</div>
						<?php endwhile; ?>
					</div>
				</div>
			</div>

		</div>
		<section class="content--case-studies" style="text-align:center">
			<a href="<?php echo get_post_type_archive_link('casestudies'); ?>" class="btn  btn--case-study-link" >View Our Latest Case Studies</a>
		</section>
	</section>
	<?php endif ?>



	<!-- gradient + image bg block -->

	<section class="content  content--padded--2x  content--bg-img  content--gradient">
		<div class="wrapper--thin  wrapper  wrapper--central">
			<div class="backdrop-text">
			<?php the_field('backdrop_text') ?>
			</div>
		</div>
	</section>

	<!-- tiled polygon background image -->
	<?php //get_template_part('includes/featured-tiles'); ?>

<?php get_footer(); ?>