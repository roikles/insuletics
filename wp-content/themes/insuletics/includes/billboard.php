<?php 
/**
 * Big Billboard
 */
 
 if ((is_page() OR is_single())) :

	$billboard_title = get_field('billboard_title'); 
	$billboard_description = get_field('billboard_text');

	// If no title is defined, revert to using post title + location (Brinklow + Warwickshire)
	if (!$billboard_title && get_field('location')) { 
		$billboard_title = get_the_title() . ', ' . get_field('location');
	}
	// If no billboard text is defined, revert to using project description
	if (!$billboard_description && get_field('description')) {
		$billboard_description = get_field('description');
	}

 	if (!get_field('use_multiple_images')) :
		$billboard_img_id = get_field('background_image');
		$size = "billboard"; // thumbnail, medium, large, full, custom_size or array(x,y)
		$billboard_img_src = wp_get_attachment_image_src( $billboard_img_id, $size);
?>
		<div class="billboard-container">
			<section class="billboard  billboard--slide" style="background-image: url(<?php echo $billboard_img_src[0]; ?>)">
				<div class="billboard__background"></div>
				<div class="billboard__caption">
					<div class="wrapper  wrapper--central  wrapper--thin">
						<h1 class="billboard__title"><?php echo $billboard_title; ?></h1>
						<span class="billboard__description"><?php echo $billboard_description; ?></span>
					</div>
				</div>
			</section>
		</div>

<?php else: ?>
		
		<div id="billboardSlider" class="billboard-container">
		<?php 
			$images = get_field('background_slider'); 
			if ($images) :
				foreach ($images as $image) :
		?>
				<section class="billboard  billboard--slide" style="background-image: url(<?php echo $image['sizes']['billboard']; ?>)">
					<div class="billboard__background"></div>
					<div class="billboard__caption">
						<div class="wrapper  wrapper--central  wrapper--thin">
							<h1 class="billboard__title"><?php echo $billboard_title; ?></h1>
							<span class="billboard__description"><?php echo $billboard_description; ?></span>
						</div>
					</div> 
				</section>
		<?php
				endforeach;
			endif;
		?>
		</div>

<?php endif; 
endif;