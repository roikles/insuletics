<?php get_header(); ?>

<?php get_sidebar('main'); ?>
<div class="container">

	<!-- Standard content area, white bg with  text and images -->

	<section class="content  content--padded">
		<div class="wrapper  wrapper--central">

			<div class="grid">

				<div class="col--one-whole  col--three-quarters--l">
					<h1><?php echo get_the_date( 'F Y' ); ?></h1>
					<?php if (have_posts()): while(have_posts()): the_post(); ?>
						<h2><?php the_title(); ?></h2>
						<?php the_content(); ?>
					<?php endwhile; endif; ?>
				</div>

				<?php get_sidebar('news'); ?>
			</div>
		</div>
	</section>
<?php get_footer(); ?>