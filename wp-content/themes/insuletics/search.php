<?php
global $query_string;

$query_args = explode("&", $query_string);
$search_query = array();

if ($query_args[0]) {
    foreach($query_args as $key => $string) {
        $query_split = explode("=", $string);
        $search_query[$query_split[0]] = urldecode($query_split[1]);
    } // foreach
}

$search = new WP_Query($search_query);

global $wp_query;
$total_results = $wp_query->found_posts;

/* Paginate results */

$big = 999999999; // need an unlikely integer
$args = array(
    'base'         => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
    'format'       => '?paged=%#%',
    'total'        => $wp_query->max_num_pages,
    'current'      => max(1, get_query_var('paged')),
    'show_all'     => False,
    'end_size'     => 1,
    'mid_size'     => 2,
    'prev_next'    => True,
    'prev_text'    => __('Previous'),
    'next_text'    => __('Next'),
    'type'         => 'plain',
    'add_args'     => False,
    'add_fragment' => ''
);
?>

<?php get_header(); ?>

<?php get_sidebar('main'); ?>
<div class="container  clear">

    <!-- Include Billboard -->
    <div class="billboard-container">
        <section class="billboard" style="background-image: url(<?php assets('imgs'); ?>/billboards/search-billboard.jpg)">
            <div class="billboard-helper">
                <div class="wrapper  wrapper--central  wrapper--thin">
                    <h1 class="billboard__title">Search Results</h1>
                    <span class="billboard__description">( <?php echo $total_results; ?> found )</span>
                </div>
            </div>
        </section>
    </div>

    <!-- Standard content area, white bg with  text and images -->

    <section class="content  content--padded">
        <div class="wrapper  wrapper--central">

            <div class="grid">
                <div class="col--one-whole  col--two-thirds--l  col--three-quarters--xl">
                    <section class="search-results"> 

                        <?php if (!empty($search_query['s'])) : ?>

                            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                                <article class="search-result">
                                    <h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
                                    <?php the_excerpt(); ?>
                                </article>
                                
                            <?php endwhile; ?>

                                <div class="pagination pagination--search-result">
                                    <?php echo paginate_links($args); ?> 
                                </div>

                            <?php endif; ?>

                        <?php else : $total_results = 0; ?>

                            <h1>Search Results ( <?php echo $total_results; ?> found )</h1>
                            <p>Sorry, there are no results matching your search criteria</p>

                        <?php endif; ?>     

                        </section>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>