<?php get_header(); ?>

<?php get_sidebar('main'); ?>
<div class="container">

	<!-- Standard content area, white bg with  text and images -->

	<section class="content  content--padded">
		<div class="wrapper  wrapper--central">

			<div class="grid">

				<div class="col--one-whole  col--three-quarters--l">
					<h1>404 Page not found</h1>
					<p>Woops! The page you are looking for doesn't exist anymore or has been moved.</p>
					<p>Why not try searching for something?</p>
					<form role="search" method="get" action="<?php echo home_url('/'); ?>">
			            <input type="text" value="" name="s" class="search__input" placeholder="Search Insuletics">
			            <input type="submit" value="Search" name="search-button" class="search__button">
			        </form>
				</div>

				<aside class="col--one-whole  col--one-quarter--l">
					<div class="secondary-nav">
						
						<!-- Call to action phone box -->
						<div class="call-to-action  call-to-action--green">
							<h2>Phone</h2>
							<p>01782 366 090</p>
						</div>

						<div class="sidebar__block">
							<p class="sidebar__email">Email: <a href="mailto:enquiries@insuletics.com">enquiries@insuletics.com</a></p>
						</div>

						<div class="sidebar__block">
							<h3>Where to Find us</h3>
							<p>Insuletics Ltd</p>
							<p>Genesis Centre</p>
							<p>North Staffs Business Park</p>
							<p>Innovation Way</p>
							<p>Stoke-on-Trent</p>
							<p>ST6 4BF</p>
						</div>
						
					</div>
				</aside>

			</div>
		</div>
	</section>
<?php get_footer(); ?>