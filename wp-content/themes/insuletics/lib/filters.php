<?php

/**
 * Remove Paragraphs Around Images
 * (the_content)
 */

function flexbones_remove_img_p($content){
   return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}

add_filter('the_content', 'flexbones_remove_img_p');

/**
 * Prevent Admin Editor removing br's
 */

function flexbones_preserve_brs($init) {
    $init['remove_linebreaks'] = false; 
    return $init;
}

add_filter('tiny_mce_before_init', 'flexbones_preserve_brs');

/**
 * Hide Admin Bar
 */

add_filter('show_admin_bar', '__return_false');

/**
 * Remove bloat from wordpress html head
 */

function flexbones_trim_head(){
    remove_action('wp_head', 'rsd_link');
    remove_action('wp_head', 'wp_generator');
    remove_action('wp_head', 'feed_links');
    remove_action('wp_head', 'feed_links', 2);
    remove_action('wp_head', 'index_rel_link');
    remove_action('wp_head', 'wlwmanifest_link');
    remove_action('wp_head', 'feed_links_extra', 3);
    remove_action('wp_head', 'start_post_rel_link', 10, 0);
    remove_action('wp_head', 'parent_post_rel_link', 10, 0);
    remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
}

add_action( 'init', 'flexbones_trim_head' );

/**
 * Remove recent comment inline CSS
 */

function flexbones_remove_inline_css() {
    global $wp_widget_factory;
    remove_action( 'wp_head', array( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'  ) );
}

add_action( 'widgets_init', 'flexbones_remove_inline_css' );


/**
 * Add custom body classes conditionally
 */
function custom_body_classes( $classes ) {

    if (is_page('contact')) {
        $classes[] = 'page-contact-us';
    }

    if ((is_page() OR is_single() OR (get_post_type()=='casestudies')) && get_field('background_image')) {
        $classes[] = 'has-billboard';
    }else{
        $classes[] = 'no-billboard';
    }

    if (in_category('news')) {
        $classes[] = 'news-article';
    }

    return $classes;
}

add_filter( 'body_class', 'custom_body_classes' );

/**
 * Rename "Posts" to "News"
 * @link http://new2wp.com/snippet/change-wordpress-posts-post-type-news/
 */

function pilau_change_post_menu_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'News';
    $submenu['edit.php'][5][0] = 'News';
    $submenu['edit.php'][10][0] = 'Add News';
    $submenu['edit.php'][16][0] = 'News Tags';
    echo '';
}
function pilau_change_post_object_label() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'News';
    $labels->singular_name = 'News';
    $labels->add_new = 'Add News';
    $labels->add_new_item = 'Add News';
    $labels->edit_item = 'Edit News';
    $labels->new_item = 'News';
    $labels->view_item = 'View News';
    $labels->search_items = 'Search News';
    $labels->not_found = 'No News found';
    $labels->not_found_in_trash = 'No News found in Trash';
}

add_action( 'admin_menu', 'pilau_change_post_menu_label' );
add_action( 'init', 'pilau_change_post_object_label' );


// filter the Gravity Forms button type
function form_submit_button($button, $form){

    switch ($form["id"]) {
        case 1: // Contact Form
            $text = "Send Enquiry";
            break;

        case 2: // Recruitment Form
            $text = "Send CV";
            break;
        
        default: // All other forms
            $text = "Submit";
            break;
    }

    return "<button class='btn' id='gform_submit_button_{$form["id"]}'>" . $text . "</button>";
}

add_filter("gform_submit_button", "form_submit_button", 10, 2);