<section class="sidebar" id="sidebarSlideOut">
    
    <div class="sidebar__logo">
        <img src="<?php assets('imgs');?>/insuletics-logo.svg" alt="insuletics logo">
    </div>

    <nav class="sidebar__nav">
        <?php bem_menu("main_menu", "main-menu") ?>
    </nav>

    <div class="sidebar__search">
        <form role="search" method="get" action="<?php echo home_url('/'); ?>">
            <input type="text" value="" name="s" class="search__input search__input--sidebar" placeholder="search insuletics">
            <input type="submit" value="Search" name="search-button" class="search__button search__button--sidebar">
        </form>
    </div>

    <div class="sidebar__contact">
        
        <div class="contact">
            <ul class="contact__item  contact__item--email">
                <li><h2>Email Enquiries</h2></li>
                <li>enquiries@insuletics.com</li>
            </ul>
            <ul class="contact__item  contact__item--phone">
                <li><h2>Phone</h2></li>
                <li>01782 366 090</li>
            </ul>
            <ul class="contact__item  contact__item--address">   
                <li><h2>Insuletics ltd</h2></li>
                <li>Genesis Centre</li>
                <li>North Staffs Business Park</li>
                <li>Innovation Way</li>
                <li>Stoke-on-Trent</li>
                <li>ST6 4BF</li>
            </ul>
<!--             <a href="<?php echo get_permalink(24); ?>" class="btn  btn--default  btn--medium  contact__btn">Get in touch</a>
 -->        </div>
        
    </div>

</section>