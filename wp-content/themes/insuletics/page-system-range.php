<?php get_header(); ?>

<?php get_sidebar('main'); ?>
<div class="container  clear">

	<!-- Include Billboard -->
	<?php get_template_part('includes/billboard'); ?>

	<!-- Standard content area, white bg with  text and images -->

	<section class="content  content--padded">
		<div class="wrapper  wrapper--central">

			<div class="grid">
				<div class="col--one-whole">
					<div class="content-container">

						<?php 
						/**
						 * This template contains the ACF logic for the System Finishes Page
						 */
						if (have_rows('insuletics_finishes')) : while(have_rows('insuletics_finishes')) : the_row(); 
							$img		= wp_get_attachment_image_src(get_sub_field('texture'), array(50,50));
							$img_src 	= $img[0];
						?>	

							<section class="system-finish">
								<div class="system-finish--title  clear">
									<img src="<?php echo $img_src; ?>" alt="<?php the_sub_field('name'); ?> sample"/>
									<h2><?php the_sub_field('name'); ?></h2>
								</div>
								<div class="grid">
									<div class="col--one-whole  col--three-quarters--l">
										<?php the_sub_field('description'); ?>
									</div>
								</div>

								<div class="grid">			
									<div class="col--omega--l  col--one-whole  col--one-third--l  col--one-half--xl">
										<p><strong>Available Primer / Base Coat:</strong><br>
										<?php the_sub_field('primer_/_base_coat'); ?></p>

										<p><strong>Available Top Coat:</strong><br>
										<?php the_sub_field('top_coat'); ?></p>
									</div>
									<div class="col--one-whole  col--two-thirds--l  col--one-half--xl">
										<?php if (have_rows('product_table_rows')): while (have_rows('product_table_rows')): the_row(); 
												$thumb		= wp_get_attachment_image_src(get_sub_field('3d_render'), 'thumbnail' );
												$thumb_src  = $thumb[0];

												$thumb_large = wp_get_attachment_image_src(get_sub_field('3d_render'), 'full' );
												$thumb_large_src = $thumb_large[0];		
										?>
											<div class="finish-data  clear">
												<div class="grid">
													<div class="col--one-whole  col--one-half--m">
														<h3><?php the_sub_field('product'); ?></h3>
														<p><strong>Insulation Type:</strong><br>
														<?php 
															if (get_sub_field('insulation_type')): 
																the_sub_field('insulation_type'); 
															endif;
														?>
														</p>
														<?php if (get_sub_field('bba')): ?>
															<p><a href="<?php the_sub_field('bba'); ?>" title="Download the BBA Certificate for <?php the_sub_field('product'); ?>" target="_blank">View the BBA Certificate</a></p>
														<?php endif; ?>
													</div>

													<?php if (get_sub_field('3d_render')): ?>
													<div class="col--one-whole  col--one-half--m">
														<a href="<?php echo $thumb_large_src; ?>" target="_blank" class="finish-data--thumb">
															<img src="<?php echo $thumb_src; ?>" alt="<?php the_sub_field('product'); ?> 3D Render">
															<small>Click to enlarge</small>
														</a>
													</div>
													<?php endif; ?>
												</div>
											</div>
										<?php endwhile; endif ?>
									</div>
								</div>
							</section>

						<?php endwhile; endif; ?>

						<div class="grid">
							<div class="col--one-whole  col--two-thirds--l">
								<h2>Other Finishes</h2>
								<?php
								if (get_field('misc_finishes')) {
									the_field('misc_finishes');
								}
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php get_footer(); ?>