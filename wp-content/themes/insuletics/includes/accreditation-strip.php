<?php 
/**
 * Accreditation strip to be used within the footer
 * Has multiple logos within a carousel
 * - Found in footer.php
 */

 ?>
<div class="accreditations  clear">
	<?php if(have_rows('accreditation_logos','option')): ?>
		<ul id="lightCarousel" class="carousel  clear">
		<?php while(have_rows('accreditation_logos','option')): the_row(); ?>
			<li>
				<div class="single-accreditation">			
					<a href="<?php the_sub_field('accreditation_link'); ?>" target="_blank" class="single-accreditation__link">
						<?php 
							$image = get_sub_field('accreditation_image'); 
							$image_html = ($image ? "<img src='" . $image['sizes']['thumbnail'] . "' alt=" . get_sub_field('accreditation_link') . "/>" : "No image defined");
							echo $image_html;
						?>
					</a>
				</div>
			</li>
		<?php endwhile; ?>
		</ul>
	<?php endif; ?>	
</div>