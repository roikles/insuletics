module.exports = function(grunt) {
    "use strict";

    grunt.initConfig({
        wordpressdeploy: {
            options: {
                backup_dir: "backups/",
                rsync_args: ['--verbose', '--progress', '-rlpt', '--compress', '--omit-dir-times', '--delete'],
                exclusions: ['Gruntfile.js', '.git/', 'tmp/*', 'backups/', 'composer.json', 'composer.lock', 'README.md', '.gitignore', 'package.json', 'node_modules']
            },
            local: {
                "title": "local",
                "database": "insuletics",
                "user": "root",
                "pass": "root",
                "host": "localhost",
                "url": "http://localhost/insuletics",
                "path": "/Applications/MAMP/htdocs/insuletics"
            },
            staging: {
                "title": "staging",
                "database": "insuletics",
                "user": "digital",
                "pass": "0987poiu",
                "host": "staging.codebluedigital.com",
                "url": "http://staging.codebluedigital.com/insuletics",
                "path": "/var/www/staging.codebluedigital.com", //no project folder required, takes current folder name...I think
                "ssh_host": "digital@staging.codebluedigital.com"
            }
        }
    });

    // Load tasks
    grunt.loadNpmTasks('grunt-wordpress-deploy');

    // Register tasks
    grunt.registerTask('default', 'wordpressdeploy:staging');

    grunt.registerTask('deploy', 'Deploy to server environment', function(target) {
        grunt.option('target', target);
        grunt.task.run('push_files');
        grunt.task.run('push_db');
    });

};