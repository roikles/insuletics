<?php

/**
 * Add Custom Post Types to WordPress
 */

add_action('init', 'flexbones_cpts');

function flexbones_cpts() {
    $post_type_name = "Case Studies";
    $single_name    = "Case Study";
    $plural_name    = "Case Studies";
    $icon           = "dashicons-images-alt";
    $description    = "Insuletics Ltd Case Studies";

    $post_type_options = array(
        'label'                 => $single_name,
        'description'           => $description,
        'public'                => true,
        'menu_icon'             => $icon,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'capability_type'       => 'post',
        'hierarchical'          => true,
        'rewrite'               => array(
            'slug'                  => '',
            'with_front'            => '0'
        ),
        'query_var'             => true,
        'has_archive'           => true,
        'supports'              => array(
            'title',
            'editor',
            'revisions',
            'thumbnail',
            'author',
            'page-attributes',
        ),
        'taxonomies' => array('category'),
        'labels'                => array(
            'name'                  => $plural_name,
            'singular_name'         => $single_name,
            'menu_name'             => $plural_name,
            'add_new'               => 'Add '.$plural_name,
            'add_new_item'          => 'Add New '.$single_name,
            'edit'                  => 'Edit',
            'edit_item'             => 'Edit '.$single_name,
            'new_item'              => 'New '. $single_name,
            'view'                  => 'View '.$plural_name,
            'view_item'             => 'View '.$single_name,
            'search_items'          => 'Search '.$plural_name,
            'not_found'             => 'No '. $plural_name . ' Found',
            'not_found_in_trash'    => 'No '.$plural_name.' Found in Trash',
            'parent'                => 'Parent '. $plural_name
        ),
    );
    register_post_type($post_type_name, $post_type_options);
}