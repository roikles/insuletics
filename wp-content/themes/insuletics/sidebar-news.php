<aside class="col--one-whole  col--one-quarter--l">
	<div class="sidebar-container">
		<div class="secondary-nav">

			<div class="sidebar__block">
				<h2>Recent Posts</h2>
				<ul>
				<?php 
					$recent = wp_get_recent_posts( array('numberposts' => 5), OBJECT );
					foreach ($recent as $post) : setup_postdata( $post );
				?>
				
						<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

				<?php endforeach; wp_reset_postdata(); ?>				
				</ul>		
			</div>

			<div class="sidebar__block">
				<h2>News Archive</h2>
				<ul>
				<?php 
					$args 		= array(
							'limit' 	=> 	24,
							'format'	=> 	'custom',
							'before'	=>	'<li>',
							'after'		=>	'</li>'
					);

					wp_get_archives( $args ); 
				?>
				</ul>
			</div>
		</div>
	</div>
</aside>