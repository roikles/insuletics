/**
 * Maps
 */

// Only load if we are on the contact page

function initialize() {
	var mapOptions = {
		center: new google.maps.LatLng(53.061874,-2.224770),
		zoom: 15,
		disableDefaultUI: true,
		zoomControlOptions: {
			style: google.maps.ZoomControlStyle.SMALL,
			position: google.maps.ControlPosition.LEFT_TOP
		},
		scrollwheel: false,
	};

	function setMarker(map) {
		var imgroot = stylesheet_root.dir + '/assets/imgs/';
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(53.061874,-2.224770),
			map: map,
			icon: imgroot + 'map-pin.png',
			title: "Insuletics Ltd"
		});

	}

	if($("#mapCanvas").length > 0){
		var map = new google.maps.Map(document.getElementById("mapCanvas"),mapOptions);
	}else{
		return false;
	}

	setMarker(map);

	var center;
	function calculateCenter() {
		center = map.getCenter();
	}
	google.maps.event.addDomListener(map, 'idle', function() {
		calculateCenter();
	});
	google.maps.event.addDomListener(window, 'resize', function() {
		map.setCenter(center);
	});
}

if ($("body").hasClass('page-contact-us')) {
	google.maps.event.addDomListener(window, 'load', initialize);
};
