<?php get_header(); the_post(); ?>

<?php get_sidebar('main'); ?>
<div class="container">

	<!-- Include Map instead of Billboard -->
	<div class="billboard-container">
		<section class="billboard" id="mapCanvas">
			<div class="billboard-helper">
				<div class="wrapper  wrapper--central  wrapper--thin">
					<h1 class="billboard__title">Come find us</h1>
				</div>
			</div>
		</section>
	</div>

	<!-- Standard content area, white bg with  text and images -->

	<section class="content  content--padded">
		<div class="wrapper  wrapper--central">

			<div class="grid">

				<div class="col--one-whole  col--two-thirds--l  col--three-quarters--xl">
					<?php the_content(); ?>
				</div>

				<aside class="col--one-whole  col--one-third--l col--one-quarter--xl">
					<div class="secondary-nav">
						
						<!-- Call to action phone box -->
						<div class="call-to-action  call-to-action--green">
							<h2>Phone</h2>
							<p>01782 366 090</p>
						</div>

						<div class="sidebar__block">
							<p class="sidebar__email">Email: <a href="mailto:enquiries@insuletics.com">enquiries@insuletics.com</a></p>
						</div>

						<div class="sidebar__block">
							<h3>Where to Find us</h3>
							<p>Insuletics Ltd</p>
							<p>Genesis Centre</p>
							<p>North Staffs Business Park</p>
							<p>Innovation Way</p>
							<p>Stoke-on-Trent</p>
							<p>ST6 4BF</p>
						</div>
						
					</div>
				</aside>

			</div>
		</div>
	</section>

<?php get_footer(); ?>