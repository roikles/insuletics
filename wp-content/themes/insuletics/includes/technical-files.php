<?php
/**
 * Technical Support Documents ACF loop
 */
if (have_rows('groups')) : while(have_rows('groups')): the_row();

	$groupname = get_sub_field('group-title');
?>
	<h2><?php echo $groupname; ?></h2>
	<div class="grid">

	<?php
		if (have_rows('downloads-tech-support')) : while(have_rows('downloads-tech-support')): the_row(); 
		$title = get_sub_field('title');
		$file  = get_sub_field('file');
	?>

		<div class="col--one-whole  col--one-half--m">

			<div class="single-download">

				<div class="grid">

					<div class="col--one-sixth  col--one-quarter--m">
						<a href="<?php echo $file; ?>" class="single-download__link">
							<img src="<?php assets('imgs'); ?>/download-icon.png" alt="Download <?php echo $title; ?>">
						</a>
					</div>

					<div class="col--five-sixths  col--three-quarters--m">
						<a href="<?php echo $file; ?>" class="single-download__link">
							<p class="single-download__title"><?php echo $title; ?></p>
						</a>
					</div>

				</div>

			</div>

		</div>

	<?php endwhile; endif; ?>

	</div>

<?php endwhile; else: ?>
	<p>Sorry, there are currently no documents available.</p>
<?php endif; 