<!-- Footer accreditations -->
<?php get_template_part('includes/accreditation-strip'); ?>

<footer class="clear">
	<section class="content  content--padded">
		<div class="wrapper  wrapper--central">

			<div class="grid">

				<!-- Footer Column -->
				<div class="col--one-whole  col--one-third--m  col--one-quarter--xl">
					<div class="footer-block  footer-block--tech-support">
						<h3>Technical Support</h3>
						<ul>
						<?php 
							$args			= array(
								'child_of'		=>	12,
								'post_status'	=>	'publish',
								'title_li'		=>	''
							);
							$child_pages 	= wp_list_pages( $args ); 
						?>
						</ul>
					</div>
				</div>

				<!-- Footer Column -->
				<div class="col--one-whole  col--one-third--m  col--two-twelfths--xl">
					<div class="footer-block  footer-block--systems">
						<h3>Our System</h3>
						<p><a href="<?php echo get_permalink(10); ?>">System Components</a></p>
						<p><a href="<?php echo get_permalink(242); ?>">System Finishes</a></p>
					</div>

					<div class="footer-block  footer-block--downloads">
						<h3>Downloads</h3>
						<p><a href="<?php echo get_permalink(20); ?>/#qf_brochures">Brochures</a></p>
						<p><a href="<?php echo get_permalink(20); ?>/#qf_certifications">Certifications</a></p>
						<p><a href="<?php echo get_permalink(20); ?>/#qf_policies">Policies</a></p>
					</div>
				</div>

				<!-- Footer Column -->
				<div class="col--one-whole  col--one-third--m  col--one-quarter--xl">
					<div class="footer-block  footer-block--news">
						<h3>Latest News</h3>
						<ul>
							<?php 
								$recent = wp_get_recent_posts( array('numberposts' => 3), OBJECT );
								foreach ($recent as $post) : setup_postdata( $post );
							?>
								<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
							<?php endforeach; wp_reset_postdata(); ?>               
						</ul>
					</div>
				</div>

				<!-- Footer Column -->
				<div class="col--one-whole  col--one-third--m  col--one-quarter--xl">
					<div class="footer-block  footer-block--accreditations">
						<h3>Terms</h3>
						<p><a href="<?php echo get_permalink(287); ?>">Terms &amp; Conditions</a></p>
						<p>Built by <?php echo agency_link('http://www.codebluedigital.com','Code Blue Digital','http://www.insuletics.com'); ?></p>
					</div>
				</div>

			</div>
		</div>
	</section>
</footer>
</section>
<?php wp_footer(); ?>
<script>
	/**
	 * Google Analytics
	 * Just add a tracking code and you are set!
	 */

	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-54427164-1', 'auto');
	ga('send', 'pageview');
	
</script>
</body>
</html>